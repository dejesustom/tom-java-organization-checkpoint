package com.galvanize;

public class Booking {

    private Type roomType;
    private String roomNumber;
    private String startTime;
    private String endTime;

    public enum Type{
        r, s, a, c
    }

    public Booking(Type roomType, String roomNumber, String startTime, String endTime) {

        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }


//    [room type][room-number]-[start time]-[end time]
    public static Booking parse(String bookingCode) {

//        r111-08:30am-11:00am
        String[] parts = bookingCode.split("-");
        Type roomType = Type.valueOf(parts[0].substring(0, 1));
        String roomNumber = parts[0].substring(1);
        String startTime = parts[1];
        String endTime = parts[2];
        return new Booking(roomType, roomNumber, startTime, endTime);
    }

    public Type getRoomType() {
        return roomType;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }
}
