package formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter{


    @Override
    public String format(Booking Booking) {
        StringBuilder json = new StringBuilder();
        json.append("{\n");
        json.append("  \"type\": \"Suite\",\n");
        json.append("  \"roomNumber\": 111,\n");
        json.append("  \"startTime\": \"08:30am\",\n");
        json.append("  \"endTime\": \"11:00am\"\n");
        json.append("}");
        return json.toString();
    }
}
