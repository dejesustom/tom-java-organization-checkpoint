package formatters;

import com.galvanize.Booking;

public class HTMLFormatter implements Formatter{

    @Override
    public String format(Booking Booking) {
        StringBuilder html = new StringBuilder();
        html.append("<dl>\n");
        html.append("  <dt>Type</dt><dd>Conference Room</dd>\n");
        html.append("  <dt>Room Number</dt><dd>111</dd>\n");
        html.append("  <dt>Start Time</dt><dd>08:30am</dd>\n");
        html.append("  <dt>End Time</dt><dd>11:00am</dd>\n");
        html.append("</dl>");
        return html.toString();
    }
}
