package formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{


    @Override
    public String format(Booking booking) {
        StringBuilder csv = new StringBuilder();
        csv.append("type,room number,start time,end time\n");
        csv.append(booking.getRoomType()).append(",");
        csv.append(booking.getRoomNumber()).append(",");
        csv.append(booking.getStartTime()).append(",");
        csv.append(booking.getEndTime());
        return csv.toString();
    }
}
