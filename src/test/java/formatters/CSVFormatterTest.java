package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVFormatterTest {

    @Test
    void format() {
        //Arrange
        Formatter formatter = new CSVFormatter();
        Booking booking = new Booking(Booking.Type.r, "111", "08:30am", "11:00am");
        String expected = "type,room number,start time,end time\n" +
                          "r,111,08:30am,11:00am";
        //Act
        String actual = formatter.format(booking);
        //Assert
        assertEquals(expected, actual);
    }
}