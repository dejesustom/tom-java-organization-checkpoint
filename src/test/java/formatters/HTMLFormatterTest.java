package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HTMLFormatterTest {

    @Test
    void format() {
        //Arrange
        Formatter formatter = new HTMLFormatter();
        Booking booking = new Booking(Booking.Type.r, "111", "08:30am", "11:00am");
        String expected =   "<dl>\n" +
                            "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                            "  <dt>Room Number</dt><dd>111</dd>\n" +
                            "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
                            "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                            "</dl>";
        //Act
        String actual = formatter.format(booking);
        //Assert
        assertEquals(expected, actual);
    }
}