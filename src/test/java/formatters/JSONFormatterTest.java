package formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JSONFormatterTest {

    @Test
    void format() {
        //Arrange
        Formatter formatter = new JSONFormatter();
        Booking booking = new Booking(Booking.Type.r, "111", "08:30am", "11:00am");
        String expected =   "{\n" +
                            "  \"type\": \"Suite\",\n" +
                            "  \"roomNumber\": 111,\n" +
                            "  \"startTime\": \"08:30am\",\n" +
                            "  \"endTime\": \"11:00am\"\n" +
                            "}";
        //Act
        String actual = formatter.format(booking);
        //Assert
        assertEquals(expected, actual);
    }
}