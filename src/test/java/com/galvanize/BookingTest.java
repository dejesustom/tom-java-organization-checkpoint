package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {

    @Test
    void parseBooking() {
        //Arrange
        String bookingCode = "r111-08:30am-11:00am";
        Booking expected = new Booking(Booking.Type.r, "111", "08:30am", "11:00am");
        //Act
        Booking actual = Booking.parse(bookingCode);
        //Assert
        assertEquals(Booking.Type.r, actual.getRoomType());
        assertEquals("111", actual.getRoomNumber());
        assertEquals("08:30am", actual.getStartTime());
        assertEquals("11:00am", actual.getEndTime());
    }
}